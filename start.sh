#!/bin/bash

set -eu

echo "=> Ensure directories"
mkdir -p /run/nginx /app/data /run/prosody/conf.d/ /run/prosody/ca-certificates /app/data/prosody /run/jitsi/jicofo /run/jitsi/videobridge

echo "=> Create configs"
AUTH_METHOD="anonymous"
if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
    AUTH_METHOD="cyrus"

    echo "==> Configuring SASLauthd for LDAP"
    rm -rf /run/saslauthd.conf
    # http://www.cyrusimap.org/sasl/sasl/pwcheck.html
    echo -e "ldap_servers: ${CLOUDRON_LDAP_URL}" >> /run/saslauthd.conf
    echo -e "ldap_bind_dn: ${CLOUDRON_LDAP_BIND_DN}" >> /run/saslauthd.conf
    echo -e "ldap_bind_pw: ${CLOUDRON_LDAP_BIND_PASSWORD}" >> /run/saslauthd.conf
    echo -e "ldap_auth_method: bind" >> /run/saslauthd.conf
    echo -e "ldap_search_base: ${CLOUDRON_LDAP_USERS_BASE_DN}" >> /run/saslauthd.conf
    echo -e "ldap_filter: username=%u" >> /run/saslauthd.conf
fi

echo "==> Configuring prosody"
sed -e "s/##APP_DOMAIN/${CLOUDRON_APP_DOMAIN}/" \
    -e "s/##AUTH_METHOD/${AUTH_METHOD}/" \
    -e "s/##TURN_SECRET/${CLOUDRON_TURN_SECRET}/" \
    -e "s/##TURN_TLS_PORT/${CLOUDRON_TURN_TLS_PORT}/" \
    -e "s/##STUN_TLS_PORT/${CLOUDRON_STUN_TLS_PORT}/" \
    -e "s/##STUN_SERVER/${CLOUDRON_STUN_SERVER}/" \
    -e "s/##TURN_SERVER/${CLOUDRON_TURN_SERVER}/" \
    /app/pkg/prosody.cfg.lua > /run/prosody/conf.d/${CLOUDRON_APP_DOMAIN}.cfg.lua

echo "==> Configuring jicofo"
cp /app/pkg/jicofo.logging.properties /run/jitsi/jicofo/logging.properties
sed -e "s/##APP_DOMAIN/${CLOUDRON_APP_DOMAIN}/" \
    /app/pkg/jicofo.conf.template > /run/jitsi/jicofo/jicofo.conf
sed -e "s/##APP_DOMAIN/${CLOUDRON_APP_DOMAIN}/" \
    /app/pkg/jicofo-sip-communicator.properties > /run/jitsi/jicofo/sip-communicator.properties

echo "==> Configuring jvb"
cp /app/pkg/jvb.logging.properties /run/jitsi/videobridge/logging.properties
sed -e "s/##APP_DOMAIN/${CLOUDRON_APP_DOMAIN}/" \
    -e "s/##STUN_TLS_PORT/${CLOUDRON_STUN_TLS_PORT}/" \
    -e "s/##STUN_SERVER/${CLOUDRON_STUN_SERVER}/" \
    -e "s/##TCP_HARVESTER_PORT/${TCP_HARVESTER_PORT:-4443}/" \
    -e "s/##UDP_HARVESTER_PORT/${MEDIA_STREAM_PORT:-10000}/" \
    /app/pkg/videobridge-sip-communicator.properties > /run/jitsi/videobridge/sip-communicator.properties
sed -e "s/##APP_DOMAIN/${CLOUDRON_APP_DOMAIN}/" \
    -e "s/##MEDIA_STREAM_PORT/${MEDIA_STREAM_PORT:-10000}/" \
    /app/pkg/jvb.conf.template > /run/jitsi/videobridge/jvb.conf

echo "==> Configuring jitsi-meet-config.js"
node /app/pkg/patch-config.js

echo "=> Setup /etc/ssl/certs"
cp -rf /app/pkg/etc-ssl-certs /run/etc-ssl-certs

echo "=> Generating fresh SSL certs for domain ${CLOUDRON_APP_DOMAIN}"
mkdir -p /run/certs/
# there are lots of backup certs in /app/data/prosody which is probably from upstream package
rm -rf /run/certs/* /app/data/prosody/*bkp*

openssl req -x509 -newkey rsa:2048 -keyout /run/certs/${CLOUDRON_APP_DOMAIN}.key -out /run/certs/${CLOUDRON_APP_DOMAIN}.crt -days 3650 -subj /CN=${CLOUDRON_APP_DOMAIN} -nodes
openssl req -x509 -newkey rsa:2048 -keyout /run/certs/auth.${CLOUDRON_APP_DOMAIN}.key -out /run/certs/auth.${CLOUDRON_APP_DOMAIN}.crt -days 3650 -subj /CN=auth.${CLOUDRON_APP_DOMAIN} -nodes
openssl req -x509 -newkey rsa:2048 -keyout /run/certs/focus.${CLOUDRON_APP_DOMAIN}.key -out /run/certs/focus.${CLOUDRON_APP_DOMAIN}.crt -days 3650 -subj /CN=focus.${CLOUDRON_APP_DOMAIN} -nodes
openssl req -x509 -newkey rsa:2048 -keyout /run/certs/conference.${CLOUDRON_APP_DOMAIN}.key -out /run/certs/conference.${CLOUDRON_APP_DOMAIN}.crt -days 3650 -subj /CN=conference.${CLOUDRON_APP_DOMAIN} -nodes
openssl req -x509 -newkey rsa:2048 -keyout /run/certs/conferenceduration.${CLOUDRON_APP_DOMAIN}.key -out /run/certs/conferenceduration.${CLOUDRON_APP_DOMAIN}.crt -days 3650 -subj /CN=conferenceduration.${CLOUDRON_APP_DOMAIN} -nodes
openssl req -x509 -newkey rsa:2048 -keyout /run/certs/breakout.${CLOUDRON_APP_DOMAIN}.key -out /run/certs/breakout.${CLOUDRON_APP_DOMAIN}.crt -days 3650 -subj /CN=breakout.${CLOUDRON_APP_DOMAIN} -nodes
openssl req -x509 -newkey rsa:2048 -keyout /run/certs/avmoderation.${CLOUDRON_APP_DOMAIN}.key -out /run/certs/avmoderation.${CLOUDRON_APP_DOMAIN}.crt -days 3650 -subj /CN=avmoderation.${CLOUDRON_APP_DOMAIN} -nodes
openssl req -x509 -newkey rsa:2048 -keyout /run/certs/internal.auth.${CLOUDRON_APP_DOMAIN}.key -out /run/certs/internal.auth.${CLOUDRON_APP_DOMAIN}.crt -days 3650 -subj /CN=internal.auth.${CLOUDRON_APP_DOMAIN} -nodes

echo "=> Running update-ca-certificates for new certs"
update-ca-certificates --verbose --fresh

echo "=> Ensuring permissions"
chown -R cloudron:cloudron /run
chown -R prosody:prosody /app/data /run/certs/ /run/prosody/ /var/run/saslauthd

echo "=> Importing certs into prosody"
prosodyctl cert import ${CLOUDRON_APP_DOMAIN} /run/certs/
prosodyctl cert import avmoderation.${CLOUDRON_APP_DOMAIN} /run/certs/
prosodyctl cert import auth.${CLOUDRON_APP_DOMAIN} /run/certs/
prosodyctl cert import focus.${CLOUDRON_APP_DOMAIN} /run/certs/
prosodyctl cert import conference.${CLOUDRON_APP_DOMAIN} /run/certs/
prosodyctl cert import conferenceduration.${CLOUDRON_APP_DOMAIN} /run/certs/
prosodyctl cert import breakout.${CLOUDRON_APP_DOMAIN} /run/certs/
prosodyctl cert import internal.auth.${CLOUDRON_APP_DOMAIN} /run/certs/

echo "=> Adding user focus and jvb to prosody"
prosodyctl register focus auth.${CLOUDRON_APP_DOMAIN} focussecret
prosodyctl register jvb auth.${CLOUDRON_APP_DOMAIN} videobridgesecret

echo "=> Subscribing focus user to auth roster"
prosodyctl mod_roster_command subscribe focus.${CLOUDRON_APP_DOMAIN} focus@auth.${CLOUDRON_APP_DOMAIN}

echo "=> Start supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i jitsi
