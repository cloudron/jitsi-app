FROM cloudron/base:4.0.0@sha256:3324a94a3b1ef045b6d41623d0b1d3b82ca1721e87a2406c1876b47cbd005d8f

RUN mkdir -p /app/pkg /home/cloudron/.sip-communicator
WORKDIR /app/pkg

RUN apt-get update && apt-get install -y openjdk-8-jre openjdk-8-jdk maven sasl2-bin libsasl2-modules-ldap lua-cyrussasl && rm -rf /etc/ssh_host_* && rm -rf /var/cache/apt /var/lib/apt/lists

# We need new prosody
RUN echo deb http://packages.prosody.im/debian $(lsb_release -sc) main | sudo tee -a /etc/apt/sources.list
RUN wget https://prosody.im/files/prosody-debian-packages.key -O- | sudo apt-key add -

RUN curl https://download.jitsi.org/jitsi-key.gpg.key | sudo sh -c 'gpg --dearmor > /usr/share/keyrings/jitsi-keyring.gpg'
RUN echo 'deb [signed-by=/usr/share/keyrings/jitsi-keyring.gpg] https://download.jitsi.org stable/' | sudo tee /etc/apt/sources.list.d/jitsi-stable.list > /dev/null

# RUN apt-get update && apt search jitsi-meet && apt search prosody && exit 1

# https://download.jitsi.org/stable/
ARG JITSI_VERSION=2.0.8044-1

# We expect a jitsi-meet-prosody postinstall error here:
# modulemanager       error   Unable to load module 'roster_command': /usr/lib/prosody/modules/mod_roster_command.lua: No such file or directory
# Failed to load module 'roster_command': /usr/lib/prosody/modules/mod_roster_command.lua: No such file or directory
RUN apt-get update && apt-get install -y lua5.2 && (apt-get install -y jitsi-meet=${JITSI_VERSION} || true) && rm -rf /etc/ssh_host_* && rm -rf /var/cache/apt /var/lib/apt/lists

RUN mv /etc/ssl/certs /app/pkg/etc-ssl-certs && ln -sf /run/etc-ssl-certs /etc/ssl/certs && \
    rm -rf /usr/local/share/ca-certificates && ln -sf /run/certs /usr/local/share/ca-certificates && \
    rm -rf /etc/prosody/certs && ln -sf /run/certs /etc/prosody/certs && \
    rm -rf /etc/saslauthd.conf && ln -sf /run/saslauthd.conf /etc/saslauthd.conf && \
    rm -rf /etc/jitsi && ln -sf /run/jitsi /etc/jitsi && \
    rm -rf /etc/prosody/conf.d && ln -sf /run/prosody/conf.d /etc/prosody/conf.d

# supervisor
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor/ /etc/supervisor/conf.d/

COPY sasl-prosody.conf /etc/sasl/prosody.conf
RUN adduser prosody sasl

RUN chown -R cloudron:cloudron /home/cloudron

COPY patch-config.js prosody.cfg.lua start.sh nginx.conf \
    jicofo.sh jicofo.logging.properties jicofo.conf.template jicofo-sip-communicator.properties \
    videobridge.sh jvb.conf.template jvb.logging.properties videobridge-sip-communicator.properties \
    /app/pkg/

CMD [ "/app/pkg/start.sh" ]
