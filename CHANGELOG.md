[0.1.0]
* Initial version

[0.2.0]
* Enable user management integration

[0.3.0]
* Disable guestmode as it behaves unexpectedly

[0.4.0]
* Fix LDAP and guestmode

[0.5.0]
* Update jitsi to 2.0.7001-1
* Update prosody to 1.0.5913-1

[0.6.0]
* Allow to customize jitsi-meet-config.js
* Add optionalSso flag to support completely open jitsi instance

[0.7.0]
* Update jitsi to 2.0.7210-1

[1.0.0]
* Update Jitsi to 2.0.7287

[1.1.0]
* Update Jitsi to 2.0.7439

[1.2.0]
* Update Jisti to 2.0.7577

[1.2.1]
* Update Jitsi to 2.0.7648

[1.2.2]
* Update Jitsi to 2.0.7882
* Add ability to also specify a TCP harvester port for improved connectivity

[1.2.3]
* Cleanup backup certs on startup

