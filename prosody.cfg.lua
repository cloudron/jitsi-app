-- OUR ADDITIONS TO THE DEFUALT

log = {
    { levels = { min = "info" }, to = "console" };
}
data_path = "/app/data/prosody"
daemonize = false

-- END OUR ADDITIONS TO THE DEFUALT

plugin_paths = { "/usr/share/jitsi-meet/prosody-plugins/" }

-- domain mapper options, must at least have domain base set to use the mapper
muc_mapper_domain_base = "##APP_DOMAIN";

external_service_secret = "##TURN_SECRET";
external_services = {
     { type = "stun", host = "##STUN_SERVER", port = 3478 },
     { type = "turn", host = "##TURN_SERVER", port = 3478, transport = "udp", secret = true, ttl = 86400, algorithm = "turn" },
     { type = "turns", host = "##TURN_SERVER", port = 5349, transport = "tcp", secret = true, ttl = 86400, algorithm = "turn" }
};

cross_domain_bosh = false;
consider_bosh_secure = true;
-- https_ports = { }; -- Remove this line to prevent listening on port 5284

-- https://ssl-config.mozilla.org/#server=haproxy&version=2.1&config=intermediate&openssl=1.1.0g&guideline=5.4
ssl = {
    protocol = "tlsv1_2+";
    ciphers = "ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384"
}

unlimited_jids = {
    "focus@auth.##APP_DOMAIN",
    "jvb@auth.##APP_DOMAIN"
}

VirtualHost "##APP_DOMAIN"
    -- enabled = false -- Remove this line to enable this host
    -- authentication = "anonymous"
    authentication = "##AUTH_METHOD"
    allow_unencrypted_plain_auth = true
    -- cyrus_application_name = "prosody"

    -- Properties below are modified by jitsi-meet-tokens package config
    -- and authentication above is switched to "token"
    --app_id="example_app_id"
    --app_secret="example_app_secret"
    -- Assign this host a certificate for TLS, otherwise it would use the one
    -- set in the global section (if any).
    -- Note that old-style SSL on port 5223 only supports one certificate, and will always
    -- use the global one.
    ssl = {
        key = "/run/certs/##APP_DOMAIN.key";
        certificate = "/run/certs/##APP_DOMAIN.crt";
    }
    av_moderation_component = "avmoderation.##APP_DOMAIN"
    speakerstats_component = "speakerstats.##APP_DOMAIN"
    conference_duration_component = "conferenceduration.##APP_DOMAIN"
    -- we need bosh
    modules_enabled = {
        "bosh";
        "pubsub";
        "ping"; -- Enable mod_ping
        "speakerstats";
        "external_services";
        "conference_duration";
        "muc_lobby_rooms";
        "muc_breakout_rooms";
        "av_moderation";
        "auth_cyrus";
    }
    c2s_require_encryption = false
    lobby_muc = "lobby.##APP_DOMAIN"
    breakout_rooms_muc = "breakout.##APP_DOMAIN"
    main_muc = "conference.##APP_DOMAIN"
    -- muc_lobby_whitelist = { "recorder.##APP_DOMAIN" } -- Here we can whitelist jibri to enter lobby enabled rooms

VirtualHost "guest.##APP_DOMAIN"
    authentication = "anonymous"
    c2s_require_encryption = false

Component "conference.##APP_DOMAIN" "muc"
    restrict_room_creation = true
    storage = "memory"
    modules_enabled = {
        "muc_meeting_id";
        "muc_domain_mapper";
        "polls";
        -- "token_verification";
        "muc_rate_limit";
    }
    admins = { "focus@auth.##APP_DOMAIN" }
    muc_room_locking = false
    muc_room_default_public_jids = true

Component "breakout.##APP_DOMAIN" "muc"
    restrict_room_creation = true
    storage = "memory"
    modules_enabled = {
        "muc_meeting_id";
        "muc_domain_mapper";
        --"token_verification";
        "muc_rate_limit";
    }
    admins = { "focus@auth.##APP_DOMAIN" }
    muc_room_locking = false
    muc_room_default_public_jids = true

-- internal muc component
Component "internal.auth.##APP_DOMAIN" "muc"
    storage = "memory"
    modules_enabled = {
        "ping";
    }
    admins = { "focus@auth.##APP_DOMAIN", "jvb@auth.##APP_DOMAIN" }
    muc_room_locking = false
    muc_room_default_public_jids = true

VirtualHost "auth.##APP_DOMAIN"
    modules_enabled = {
        "limits_exception";
    }
    authentication = "internal_hashed"
    ssl = {
        key = "/run/certs/auth.##APP_DOMAIN.key";
        certificate = "/run/certs/auth.##APP_DOMAIN.crt";
    }

-- Proxy to jicofo's user JID, so that it doesn't have to register as a component.
Component "focus.##APP_DOMAIN" "client_proxy"
    target_address = "focus@auth.##APP_DOMAIN"

Component "speakerstats.##APP_DOMAIN" "speakerstats_component"
    muc_component = "conference.##APP_DOMAIN"

Component "conferenceduration.##APP_DOMAIN" "conference_duration_component"
    muc_component = "conference.##APP_DOMAIN"

Component "avmoderation.##APP_DOMAIN" "av_moderation_component"
    muc_component = "conference.##APP_DOMAIN"

Component "lobby.##APP_DOMAIN" "muc"
    storage = "memory"
    restrict_room_creation = true
    muc_room_locking = false
    muc_room_default_public_jids = true
    modules_enabled = {
        "muc_rate_limit";
        "polls";
    }
