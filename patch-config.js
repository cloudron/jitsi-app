#!/usr/bin/env node

'use strict';

/* jshint strict: true, node: true, esnext: true */

const fs = require('fs');

const DEST_PATH = '/run/jitsi-meet-config.js';
const DOMAIN = process.env.CLOUDRON_APP_DOMAIN;
const USER_CONFIG_PATH = '/app/data/jitsi-meet-config.js';
const USER_CONFIG_TEMPLATE = `
// See https://github.com/jitsi/jitsi-meet/blob/master/config.js for available options

exports = module.exports = {
    enableNoAudioDetection: true,
    enableNoisyMicDetection: true,
    enableWelcomePage: true,

    p2p: {
        enabled: true
    }
};

`;

console.log(`Generating ${DEST_PATH}`);
console.log(`Custom values can be set at ${USER_CONFIG_PATH}`);

console.log(`Trying to read ${USER_CONFIG_PATH}`);
if (!fs.existsSync(USER_CONFIG_PATH)) {
    console.log(`Generating empty custom config at ${USER_CONFIG_PATH}`);
    fs.writeFileSync(USER_CONFIG_PATH, USER_CONFIG_TEMPLATE);
}

let config = require(USER_CONFIG_PATH);

// patching Cloudron related values
config.hosts = {
    domain: DOMAIN,
    muc: `conference.${DOMAIN}`
};

if (process.env.CLOUDRON_LDAP_SERVER) config.hosts.anonymousdomain = `guest.${DOMAIN}`;

config.bosh = `//${process.env.CLOUDRON_APP_DOMAIN}/http-bind`;
config.p2p = config.p2p || {};
config.p2p.stunServers = [
    { urls: `stun:${DOMAIN}:${process.env.CLOUDRON_STUN_TLS_PORT}` },
    { urls: 'stun:meet-jit-si-turnrelay.jitsi.net:443' }
];

console.log('Using the following jitsi-meet-config:');
console.log(require('util').inspect(config, { depth: 8 }));

let data = 'var config = ' + JSON.stringify(config) + ';';
fs.writeFileSync(DEST_PATH, data);

console.log('Done');
