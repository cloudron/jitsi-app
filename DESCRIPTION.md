### Jitsi Meet

Jitsi Meet is an open source JavaScript WebRTC application used primarily for video conferencing.
In addition to audio and video, screen sharing is available, and new members can be invited via a generated link.
The interface is accessible via web browser or with a mobile app.

#### Key features of Jitsi Meet

* Encrypted communication (secure communication): 1–1 calls use the P2P mode, which is end-to-end encrypted via DTLS-SRTP between the two participants. Group calls also use DTLS-SRTP encryption, but rely on the Jitsi Videobridge (JVB) as video router, where packets are decrypted temporarily.[41] The Jitsi team emphasizes that "they are never stored to any persistent storage and only live in memory while being routed to other participants in the meeting", and that this measure is necessary due to current limitations of the underlying WebRTC technology.
* No need of new client software installation.

### Jitsi Videobridge

Jitsi Videobridge is a video conferencing solution supporting WebRTC that allows multiuser video communication.
It is a Selective Forwarding Unit (SFU) and only forwards the selected streams to other participating users in the video conference call,
therefore, CPU horsepower is not that critical for performance.
