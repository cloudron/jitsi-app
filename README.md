## Terminology

muc - multi user chat
iq
XMPP - messaging protocol
Jingle - subprotocol that XMPP uses for establishing voice-over-ip calls or transfer files
jicofo - conference focus signaling server
SFU - selective forwarding unit
video bridge - the SFU

IQ - Info/Query (https://xmpp.org/extensions/xep-0148.html - lol)
    - https://xmpp.org/rfcs/rfc3920.html

Prosody - XMPP Server
Smack - XMPP Client

BOSH - XMPP Over HTTP (Bidirectional-streams Over Synchronous HTTP)

extensions - extend the protocol
plugins - written directly into the server (internal component). implemented as modules

Components - External via services. Prosody will listen on component_port and expect component_secret (XEP-0114)

## Debugging

The postinstall script is in `/var/lib/dpkg/info/jitsi-meet-prosody.postinst`

Run post install script as : `debconf --frontend=readline bash -x /var/lib/dpkg/info/jitsi-meet-prosody.postinst configure`

